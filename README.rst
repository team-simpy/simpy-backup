SimPy Archive
=============

This repository is an archive of old SimPy releases and the mailing list
discussions from sourceforge.net.

Mailing list
------------

The mailing list backup ``mailing_list/simpy-users.mbox`` contains all
discussions of the *simpy-users* list from September 23, 2002 until November
04, 2015.

In November 2015, we moved from sourceforge.net to `Google
Groups <https://groups.google.com/forum/#!forum/python-simpy>`_.


Releases and release dates
--------------------------

Unfortunately, we could not find any files for releases < 1.0.

======== ==========
Release  Date
======== ==========
0.1a     2002-09-17
0.2a     2002-09-23
0.2.1a   2002-09-28
0.5b     2002-10-20
0.5.1b   2002-10-22
1.0      2002-12-02
1.0.1    2002-12-09
1.2a     2003-04-13
1.2a1    2003-04-19
1.2      2003-04-29
1.3      2003-06-22
1.4      2004-02-01
1.4.1    2004-03-02
1.4.2    2004-05-25
1.5a     2004-09-30
1.5      2004-12-08
1.5.1    2005-02-02
1.6      2005-06-11
1.6.1    2005-11-19
1.7      2006-02-08
1.7final 2006-03-16
1.7.1    2006-06-15
1.8      2007-01-31
1.9(a?)  2007-12-19
1.9b     2007-12-23
1.9.0    2008-01-22
1.9.1    2008-03-18
2.0      2008-11-02
2.00     2009-01-28
2.0.1    2009-04-07
2.1.0    2010-06-02
2.2.0    2011-09-27
2.3.0    2012-02-18
======== ==========
